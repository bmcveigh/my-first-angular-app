import {Component} from '@angular/core';
/**
 * Created by brianmcveigh on 4/7/17.
 */

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styles: [`
    .online {
      color: white;
    }
  `]
})
export class ServerComponent {
  serverId = 10;
  serverStatus = 'offline';

  /**
   * Constructor for ServerComponent.
   *
   * Sets the server status to either online or offline
   * based on a random number.
   */
  constructor() {
    this.serverStatus = Math.random() > 0.5 ? 'online' : 'offline';
  }

  /**
   * Returns the status of a server as either 'online' or 'offline'.
   *
   * @returns {string}
   */
  getServerStatus() {
    return this.serverStatus;
  }

  /**
   * Returns a string containing a color name based on server status.
   * Implemented in conjunction with ngStyle.
   *
   * @returns {string|string}
   */
  getColor() {
    return this.serverStatus === 'online' ? 'green' : 'red';
  }
}
