import { Component, OnInit } from '@angular/core';

@Component({
  selector: '.app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {

  /**
   * We don't want the user to be able to create a new server.
   *
   * @type {boolean}
   */
  allowNewServer = false;

  /**
   * Users should not have the ability to reset username if it's blank.
   * @type {boolean}
   */
  allowUsernameReset = false;

  eventValue = '';

  serverCreationStatus = 'No server was created!';
  serverName = 'Test server';

  serverCreated = false;

  servers = ['Testserver', 'Testserver 2'];

  /**
   * Determine whether to show or hide the secret password text.
   *
   * @type {boolean}
   */
  shouldHideSecretPassword = true;

  /**
   * This is an array for the list of display items.
   *
   * @type {Array}
   */
  displayItemsLog = [];

  constructor() {
    // The other syntax (function() {}) would refer to the wrong "this".
    setTimeout(() => {
      this.allowNewServer = true;
    }, 1000);
  }

  ngOnInit() {
  }

  onCreateServer() {
    this.serverCreated = true;

    // Add a new server.
    this.servers.push(this.serverName);

    // This will be displayed as a message.
    this.serverCreationStatus = 'Server was created! Name is ' + this.serverName;
  }

  onUpdateServerName(event: Event) {
    // IDE needs HTMLInputElement, or else it barks at you.
    this.serverName = (<HTMLInputElement>event.target).value;
  }

  onUpdateUsername(event: Event) {
    // IDE needs HTMLInputElement, or else woof woof.
    this.eventValue = (<HTMLInputElement>event.target).value;

    if (this.eventValue.length > 0) {
      this.allowUsernameReset = true;
    } else {
      this.allowUsernameReset = false;
    }
  }

  /**
   * Will negate the shouldHideSecretPassword variable.
   */
  onClickDisplayDetails() {
    this.shouldHideSecretPassword = !this.shouldHideSecretPassword;
    this.displayItemsLog.push(new Date());
  }

}
